﻿using System;
using System.Text;
using Tramon_7;

namespace Tramon_7_via_AbstractClass {

    class AppLauncher {

        static void Main(string[] args) {

            Console.WriteLine("Task 7_1 (Shape as Abstract class):");
            Shape triangle = new Triangle(4, 4, 5);
            Console.WriteLine(triangle.getName() + " area is:\t\t" + triangle.getArea());

            Shape isoscelesTriangle = new IsoscelesTriangle(4, 5);
            Console.WriteLine(isoscelesTriangle.getName() + " area is:\t" + isoscelesTriangle.getArea());

            Shape circle = new Circle(8);
            Console.WriteLine(circle.getName() + " area is:\t\t\t" + circle.getArea());

            Shape rectangle = new Rectagle(4, 5);
            Console.WriteLine(rectangle.getName() + " area is:\t\t" + rectangle.getArea());

            Shape square = new Square(4);
            Console.WriteLine(square.getName() + " area is:\t\t\t" + square.getArea() + "\n");



            Console.WriteLine("Task 7_1 (Shape as Interface):");
            Tramon_7_1_via_Interface.Shape triangle2 = new Tramon_7_1_via_Interface.Triangle(4, 4, 5);
            Console.WriteLine(triangle2.getName() + " area is:\t\t" + triangle2.getArea());

            Tramon_7_1_via_Interface.Shape isoscelesTriangle2 = new Tramon_7_1_via_Interface.IsoscelesTriangle(4, 5);
            Console.WriteLine(isoscelesTriangle2.getName() + " area is:\t" + isoscelesTriangle2.getArea());

            Tramon_7_1_via_Interface.Shape circle2 = new Tramon_7_1_via_Interface.Circle(8);
            Console.WriteLine(circle2.getName() + " area is:\t\t\t" + circle2.getArea());

            Tramon_7_1_via_Interface.Shape rectangle2 = new Tramon_7_1_via_Interface.Rectagle(4, 5);
            Console.WriteLine(rectangle2.getName() + " area is:\t\t" + rectangle2.getArea());

            Tramon_7_1_via_Interface.Shape square2 = new Tramon_7_1_via_Interface.Square(4);
            Console.WriteLine(square2.getName() + " area is:\t\t\t" + square2.getArea() + "\n");


            Console.WriteLine("Task 7_2 (Animals Zoo):");

            Animal wolf = new Wolf();
            Animal fox = new Fox();
            Animal hare = new Hare();
            Animal deer = new Deer();

            Console.WriteLine(wolf + ":\t\t\t" + wolf.doStuff());
            Console.WriteLine(fox + ":\t\t\t" + fox.doStuff());
            Console.WriteLine(hare + ":\t\t\t" + hare.doStuff());
            Console.WriteLine(deer + ":\t\t\t" + deer.doStuff() + "\n");

            Animal[] animals = new Animal[] { wolf, fox, hare, deer, wolf, wolf, fox, deer, deer };
            Console.WriteLine(printAmountOfAnimalsByName(animals));


            Console.ReadKey();
        }

        private static String printAmountOfAnimalsByName(Animal[] animals) {

            int amountOfWolves = 0;
            int amountOfFoxes = 0;
            int amountOfHares = 0;
            int amountOfDeers = 0;
            int amountOfPredators = 0;
            int amountOfHerbivorouses = 0;
            int amountOfAnimals = 0;

            foreach (Animal animal in animals) {
                if (animal is Wolf) {
                    amountOfWolves++;
                } else if (animal is Fox) {
                    amountOfFoxes++;
                } else if (animal is Hare) {
                    amountOfHares++;
                } else if (animal is Deer) {
                    amountOfDeers++;
                }
                if (animal is Predator) {
                    amountOfPredators++;
                } else if (animal is Herbivorous) {
                    amountOfHerbivorouses++;
                }
                amountOfAnimals++;
            }

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("List of animals:\t\t");
            stringBuilder.Append("\n\t1-st level: [ Animals = " + amountOfAnimals + " ]\n" +
                "\t2-nd level: [ Predators = " + amountOfPredators + " ] " + "[ Herbivorous = " + amountOfHerbivorouses + " ]\n" +
                "\t3-rd level: [ Wolves=" + amountOfWolves + " ] " + "[ Foxes=" + amountOfFoxes + " ] " +
                "[ Hares=" + amountOfHares + " ] " + "[ Deers=" + amountOfDeers + " ]");

            return stringBuilder.ToString();
        }
    }
}