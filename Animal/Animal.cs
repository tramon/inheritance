﻿using System;

namespace Tramon_7 {

    public abstract class Animal {
        public virtual String doStuff() {
            return "do exist (animal)";
        }
    }

    public abstract class Predator : Animal {
        public override String doStuff() {
            return "do Roar!!! (Predator)";
        }
    }

    public abstract class Herbivorous : Animal {
        public override String doStuff() {
            return "do hide (herbivorous)";
        }
    }

    public class Wolf : Predator {
        public override String doStuff() {
            return "do Wof-Wof (wolf)";
        }
    }

    public class Fox : Predator {
        public override String doStuff() {
            return "do sneak (fox)";
        }
    }

    public class Hare : Herbivorous {
        public override String doStuff() {
            return "do jump (hare)";
        }
    }

    public class Deer : Herbivorous {
        public override String doStuff() {
            return "do run (deer)";
        }
    }

}
