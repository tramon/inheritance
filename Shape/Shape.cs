﻿using System;

namespace Tramon_7_via_AbstractClass {

    public abstract class Shape {
        protected double area = 0.0;
        protected String name = "Shape";

        public abstract String getName();
        public abstract double getArea();

    }

    public class Triangle : Shape {

        public Triangle(int sideA, int sideB, int sideC) {
            name = "Triangle";
            if (sideA <= 0 || sideB <= 0 || sideC <= 0) {
                Console.Error.WriteLine("Please use only argument above Zero!");
                //throw new ArgumentException();
            }

            if (sideA < sideB + sideC &&
                sideB < sideA + sideC &&
                sideC < sideA + sideB &&
                (sideA > 0 || sideB > 0 || sideC > 0)) {
                area = calculateArea(sideA, sideB, sideC);
            } else {
                Console.Error.WriteLine("\nException: Such Triangle does not exist!\n");
                //throw new ArithmeticException();
            }
        }

        public double calculateArea(int sideA, int sideB, int sideC) {
            double i = (sideA + sideB + sideC) / 2.0;
            return Math.Round(Math.Sqrt(i * (i - sideA) * (i - sideB) * (i - sideC)), 2);
        }


        public override String getName() {
            return name;
        }

        public override double getArea() {
            return area;
        }
    }

    public class IsoscelesTriangle : Triangle {

        public IsoscelesTriangle(int sides, int theBase) : base(sides, sides, theBase) {
            name = "Isosceles triangle";
            if (theBase < sides * 2 &&
                sides > 0 &&
                theBase > 0) {
                area = calculateArea(sides, theBase);
            } else {
                Console.Error.WriteLine("\nException: Such IsoscelesTriangle does not exist!\n");
                //throw new ArithmeticException();
            }
        }

        public double calculateArea(int sides, int theBase) {
            return Math.Round(theBase / 4.0 * Math.Sqrt(4.0 * Math.Pow(sides, 2) - Math.Pow(theBase, 2)), 2);
        }

    }

    public class Circle : Shape {

        public Circle(double radius) {
            name = "Circle";

            if (radius > 0) {
                area = calculateArea(radius);
            } else {
                Console.Error.WriteLine("Please use only argument above Zero!");
                //throw new ArgumentException();
            }
        }

        private double calculateArea(double radius) {
            return Math.Round(Math.PI * Math.Pow(radius, 2), 2);
        }

        public override double getArea() {
            return area; ;
        }

        public override String getName() {
            return name;
        }
    }

    public class Rectagle : Shape {
        public Rectagle(double width, double length) {
            name = "Rectangle";

            if (width > 0 && length > 0) {
                area = width * length;
            } else {
                Console.Error.WriteLine("Please use only argument above Zero!");
                //throw new ArgumentException();
            }
        }

        public override double getArea() {
            return area;
        }

        public override String getName() {
            return name;
        }
    }

    public class Square : Rectagle {

        public Square(double side) : base(side, side) {
            name = "Square";

            if (side > 0) {
                area = Math.Pow(side, 2.0);
            } else {
                Console.Error.WriteLine("Please use only argument above Zero!");
                //throw new ArgumentException();
            }



        }
    }

}


namespace Tramon_7_1_via_Interface {

    public interface Shape {
        String getName();
        double getArea();
    }

    public class Triangle : Shape {
        private String name = "Triangle";
        private double area = 0;

        public Triangle(int sideA, int sideB, int sideC) {
            if (sideA < sideB + sideC && sideB < sideA + sideC && sideC < sideA + sideB) {
                area = calculateArea(sideA, sideB, sideC);
            } else {
                Console.Error.WriteLine("\nException: Such Triangle does not exist!\n");
                //throw new ArithmeticException("Exception: Such Triangle does not exist!");
            }
        }

        public double calculateArea(int sideA, int sideB, int sideC) {
            double i = (sideA + sideB + sideC) / 2.0;
            return Math.Round(Math.Sqrt(i * (i - sideA) * (i - sideB) * (i - sideC)), 2);
        }

        public virtual String getName() {
            return name;
        }

        public virtual double getArea() {
            return area;
        }
    }

    public class IsoscelesTriangle : Triangle {
        private String name = "IsoscelesTriangle";
        private double area = 0;

        public IsoscelesTriangle(int sides, int theBase) : base(sides, sides, theBase) {
            name = "Isosceles triangle";
            if (theBase < sides * 2 &&
                sides > 0 &&
                theBase > 0) {
                area = calculateArea(sides, theBase);
            } else {
                Console.Error.WriteLine("\nException: Such IsoscelesTriangle does not exist!\n");
                //throw new ArithmeticException();
            }
        }

        public override String getName() {
            return name;
        }

        public override double getArea() {
            return area;
        }

        public double calculateArea(int sides, int theBase) {
            return Math.Round(theBase / 4.0 * Math.Sqrt(4.0 * Math.Pow(sides, 2) - Math.Pow(theBase, 2)), 2);
        }

    }

    public class Circle : Shape {
        private String name = "Shape";
        private double area = 0;


        public Circle(double radius) {
            name = "Circle";

            if (radius > 0) {
                area = calculateArea(radius);
            } else {
                Console.Error.WriteLine("Please use only argument above Zero!");
                //throw new ArgumentException();
            }
        }

        private double calculateArea(double radius) {
            return Math.Round(Math.PI * Math.Pow(radius, 2), 2);
        }

        public double getArea() {
            return area;
        }

        public String getName() {
            return name;
        }
    }

    public class Rectagle : Shape {
        private String name = "Rectagle";
        private double area = 0;


        public Rectagle(double width, double length) {
            name = "Rectangle";

            if (width > 0 && length > 0) {
                area = width * length;
            } else {
                Console.Error.WriteLine("Please use only argument above Zero!");
                //throw new ArgumentException();
            }
        }

        public virtual double getArea() {
            return area;
        }

        public virtual String getName() {
            return name;
        }
    }

    public class Square : Rectagle {
        private String name = "Square";
        private double area = 0;


        public Square(double side) : base(side, side) {
            if (side > 0) {
                area = Math.Pow(side, 2.0);
            } else {
                Console.Error.WriteLine("Please use only argument above Zero!");
                //throw new ArgumentException();
            }
        }

        public override double getArea() {
            return area;
        }

        public override String getName() {
            return name;
        }
    }

}

